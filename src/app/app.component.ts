import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'spectator-fake-file-drop-event';

  onFileChanged(e: Event) {
    console.log('onFileChanged', e)
  }
}
