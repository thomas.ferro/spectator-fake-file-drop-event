import { Component } from '@angular/core';

@Component({
  selector: 'app-my-file-input',
  templateUrl: './my-file-input.component.html',
  styleUrls: ['./my-file-input.component.css'],
  standalone: true
})
export class MyFileInputComponent {
  onFilesDrop(e: Event) {
    console.log('onFilesDrop', e)
  }
}
