import { fakeAsync, tick } from '@angular/core/testing';
import { createComponentFactory, Spectator } from '@ngneat/spectator';

import { MyFileInputComponent } from './my-file-input.component';

describe('MyFileInputComponent', () => {
  let spectator: Spectator<MyFileInputComponent>;
  const createComponent = createComponentFactory({
    component: MyFileInputComponent,
  });

  beforeEach(() => {
    spectator = createComponent();
  });

  it('should emit files when the user drops files', fakeAsync(() => {
    const emittedFiles: FileList = {
      length: 1,
      item(): File | null {
        return new File([], 'newFile.pdf');
      },
    };

    let changeEventEmitted = false;
    spectator.element.addEventListener('change', (event) => {
      const element = event.target as HTMLInputElement;
      expect(element.files!.length).toEqual(1);
      expect(element.files!.item(0)).toEqual(emittedFiles[0]);
      changeEventEmitted = true;
    });

    const dragEvent: DragEvent = {
      // @ts-ignore
      dataTransfer: {
        files: emittedFiles,
      },
    };
    spectator.triggerEventHandler('input', 'drop', dragEvent);
    tick(50);
    expect(changeEventEmitted).toBeTruthy();
  }));
});
