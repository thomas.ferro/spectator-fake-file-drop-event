import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { MyFileInputComponent } from './my-file-input/my-file-input.component';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    MyFileInputComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
